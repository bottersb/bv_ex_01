function [ img ] = my_loadImage( filename )
%MY_LOADIMAGE Loads an image with the given file.
%The image data type needs to be converted to double.
% The range needs to be vonverted from [0-255] to [0.0-1.0]

img = imread(filename);
img = double(img);
img = img ./ 255;

end

