function [ maxImg, maxHist ] = my_maxContrast( imgGray )
%MY_HISTOGRAMMAX Maximizes the image contrast.

% https://en.wikipedia.org/wiki/Normalization_(image_processing)

GRAYRANGE = 256;
grayValues = reshape(imgGray(:,:,1),[],1);

maxValue = max(grayValues);
minValue = min(grayValues);

oldRange = maxValue - minValue;
newRange = 1 / oldRange;

imgGrayTrans = imgGray - minValue;
maxImg = imgGrayTrans .* newRange;

maxHist = my_hist(maxImg);
end

