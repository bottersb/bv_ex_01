function [ imgR, imgG, imgB ] = my_RGBSplit( imgRGB )
%my_RGBSplit Splits the RGB channels of the images in separate images. 

z = zeros(size(imgRGB, 1), size(imgRGB, 2));

red = imgRGB(:,:,1);
green = imgRGB(:,:,2);
blue = imgRGB(:,:,3); 

imgR = cat(3, red, z, z);
imgG = cat(3, z, green, z);
imgB = cat(3, z, z, blue);

end

