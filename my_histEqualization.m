function [ imgHE ] = my_histEqualization( imgGray, aHist )
%MY_HISTEQUALIZATION Changes the contrast by using
%histogram equlization. The function gets the original gray image and
%its normalized accumulated histogram.

GRAYRANGE = 256;

[height,width,rgb] = size(imgGray);
%A = zeros(height,width);
%imgHE = cat(3,A,A,A);
imgHE = imgGray;
for i=1:height
    for j=1:width
        grayValue = imgGray(i,j,1);
        replaceValue = aHist(round(grayValue*GRAYRANGE));
        imgHE(i,j) = replaceValue;
%         for k=1:rgb
%             imgHE(i,j,k) = replaceValue;
%         end
    end;
end

