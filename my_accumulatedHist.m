function [ aHist ] = my_accumulatedHist( imgGray )
%MY_ACCUMULATEDHIST Calculate the NORMALIZED accumulated histogram

GRAYRANGE = 256;
grayValues = reshape(imgGray(:,:,1),[],1);

% http://martin-thoma.com/calculate-histogram-equalization/

% Histogrammm erstellen
grayHist = my_hist(imgGray);

aHist = zeros(1,GRAYRANGE);

% Akkumulieren % Normalisieren
pixel = numel(grayValues);
normFactor = 1/pixel; 
%normFactor = GRAYRANGE/pixel;
counter = 0;

for i=1:numel(grayHist)
    counter = counter + grayHist(i);
    aHist(i) = normFactor * counter; 
    %aHist(i) = round(normFactor * counter);
end;

end

