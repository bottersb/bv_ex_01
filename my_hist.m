function [ hist ] = my_hist( imgGray )
%MY_HISTOGRAM calcuates the histogram of the image.
%Remember your image contains double values between [0-1].
%You might want to convert it back to uint8 values between [0-255].

DOUBLECHECK = 0;
GRAYRANGE = 256; % 0 - 255

hist_ = zeros(1,GRAYRANGE);

grayValues = reshape(imgGray(:,:,1),[],1);
grayValues = grayValues .* (GRAYRANGE - 1); % GRAYRANGE - 1 weil Wert max 255 annehmen kann
grayValues = round(grayValues);

if DOUBLECHECK
    figure(6);
    bar(histcounts(grayValues));
    hold;
end;

for i = 1:numel(grayValues)
    grayValue = grayValues(i);
    grayValue = grayValue + 1;
    hist_(grayValue) = hist_(grayValue) + 1;
end

hist = hist_;

end

