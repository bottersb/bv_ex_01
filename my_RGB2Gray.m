function [ imgGray ] = my_RGB2Gray( img )
%MY_RGBTOGRAY converts the RGB image to a grayscale image.
% before the conversion a gamma correction is performed.

img = my_gammaCorrection(img);

red = img(:,:,1);
green = img(:,:,2);
blue = img(:,:,3);

red = red.*0.2989;
green = green.*0.5870;
blue = blue.*0.1140;

gray = red + green + blue;
imgGray = gray;                     % notwendig fuer imshow()/damit Solution von 2b funktioniert
%imgGray = cat(3,gray,gray,gray);    % notwendig fuer image()/imagesc()
end

